import { motion, AnimatePresence } from 'framer-motion';
import { LiaCheckCircle } from "react-icons/lia";

const modalAnimation = {
    initial: { opacity: 0, scale: 0.8, x: "-50%", y: "-50%" },
    animate: { opacity: 1, scale: 1, x: "-50%", y: "-50%", transition: { delay: 0.1, duration: 0.3, ease: "easeOut" } },
    exit: { opacity: 0, scale: 0.8, x: "-50%", y: "-50%", transition: { delay: 0.2, duration: 0.5, ease: "easeIn" } }
};
export function ModalSendLetter({modal,setModal}) {
  
    const handleModal = ()=>{
        setModal(!modal)
    }
    return(
       
       <AnimatePresence>
        <div className='fixed top-0 left-0 w-full h-full bg-black/25 z-[999] backdrop-blur-sm '> </div>
     <motion.div 
        className='fixed flex flex-col justify-center text-center top-[50%] sm:top-[50%] left-[50%] h-[50%] sm:h-[55%] md:h-[50%] min-w-[320px] transform-[translate(-50%, -50%)] rounded-[8px] bg-gray-500/40 z-[1000] text-white text-[20px]'
        {...modalAnimation} 
        >
            <div className=' flex justify-center items-start mb-[40px]' >
            <LiaCheckCircle  color="#4DDE00"
            className='text-[180px] animate-pulse'
            />
            </div>
           
            <p>I got your message</p>
            <p>Have a nice day</p>
            <div className=' flex justify-center mt-[20px]'>
            <button
            className=' cursor-pointer px-[2px] py-[5px] border-[1px] border-solid w-[100px] text-white rounded-xl mb-[20px] hover:bg-[#329000]'
            onClick={handleModal}
            >Ok</button>
            </div>
          
        </motion.div>
    
    </AnimatePresence>
 
    )
    
}