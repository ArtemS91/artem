import { RiArrowUpDoubleFill } from "react-icons/ri";

export function BtnScroll(){
    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
            transition:'scroll 0.9s ease-in-out'
        });
    };
    return (
        <div 
        onClick={goToTop}
        className='flex justify-center items-end  w-[100%] '>
          <span
          className='text-white text-[40px] animate-bounce shadow-lg shadow-[#E40037] rounded-full  '
          >
            <RiArrowUpDoubleFill/>
          </span>
        </div>
    )
}