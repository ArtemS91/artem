import { ModalContent } from "./ModalContent";
import WordPressTheme from "../img/Theme site.png";
import Medical from "../img/MedicalGroup.png";
import Dashboard from "../img/Dashboard.jpg";
import Crypter from "../img/Crypter.png";
import carRent from "../img/carRent.png";
import Aperture from "../img/Uber.png";
import Todo from "../img/Todo.png";
import { motion } from "framer-motion";
import { useTranslation } from "react-i18next";

const technologyWordPress = ["HTML", "CSS", "JS", "Responsive"];
const technologyMedicalGroup = [
  "HTML",
  "JS",
  "Gulp",
  "SASS",
  "AJAX",
  "Promise",
  "Modules",
  "Class",
  "Prototype",
];
const technologyCrypter = [
  "React",
  "Redux",
  "Node js",
  "Mongo DB",
  "Express JS",
  "JS",
  "CSS",
];
const technologyDashboard = [
  "React",
  "Rechart",
  "Style components",
  "TanStack Query",
  "Formik",
  "Yup",
  "Pixel Perfect",
];
const technologyTodo = ["React", "Typescript", "Tailwind", "Dnd kit", "Vite"];
const technologyTaxi = [
  "Next JS",
  "Typescript",
  "Tailwind",
  "Clerk Auth",
  "Stripe",
  "Google Maps Api",
  "React Google Places Autocomplete",
];
const technologyRent = [
  "Next JS",
  "Typescript",
  "Tailwind",
  "Clerk Auth",
  "HyGraph",
  "React-Table",
];

export function Projects() {
  const { t } = useTranslation();
  const textAnimation = {
    hidden: {
      x: -20,
      opacity: 0,
    },
    visible: (custom) => ({
      x: 0,
      opacity: 1,
      transition: { delay: 0.2 * custom },
    }),
  };
  return (
    <>
      <div 
      className="flex flex-col items-center mb-[200px] "
       id="portfolio">
        <motion.h1
          variants={textAnimation}
          initial="hidden"
          whileInView="visible"
          custom={2}
          className="text-center text-white text-[39px] font-[800] sm:text-[50px] mb-[70px] lg:text-[80px] ml-[20%] sm:ml-[15%] xl:ml-[10%] "
        >
          {t("description.projects")}
          <span className="text-[#E40037] font-[900] text-[39px] sm:text-[50px] lg:text-[70px] xl:text-[70px] ">
            .
          </span>
        </motion.h1>
        <motion.div
          variants={textAnimation}
          initial="hidden"
          whileInView="visible"
          custom={4}
          // className="text-white flex flex-wrap justify-center ml-[17%] gap-[30px] sm:gap-[10px] sm:ml-[9%] lg:gap-[30px] xl:ml-[4%]"
    className="grid grid-cols-1 sm:grid-cols-2 sm:ml-12  text-white gap-10 ml-10  "
        >
          <ModalContent
            key="Taxi-App"
            photo={Aperture}
            headerTitle="Taxi-App"
            description={t("description.ApertureTitle")}
            modalDescription={t("description.ApertureDescription")}
            linkSite="https://taxi-app-artemsitnikov.vercel.app/"
            linkCode="https://github.com/Artem91S/Uber-app"
            technology={technologyTaxi}
          />
          <ModalContent
            key="Crypter"
            photo={Crypter}
            headerTitle="Crypter"
            description={t("description.crypterTitle")}
            modalDescription={t("description.crypterDescription")}
            linkSite="https://crypter-ten.vercel.app"
            linkCode="https://github.com/Artem91S/Crypter-Final-project.git"
            technology={technologyCrypter}
          />
             <ModalContent
            key="Dashboard"
            photo={Dashboard}
            headerTitle="Dashboard"
            description={t("description.dashboardTitile")}
            modalDescription={t("description.dashboardDescription")}
            linkSite="https://dashboard-eta-lemon.vercel.app/"
            linkCode="https://github.com/Artem91S/Dashboard"
            technology={technologyDashboard}
          />
          <ModalContent
            key="CarRent"
            photo={carRent}
            headerTitle="Car Rent"
            description={t("description.rentTitle")}
            modalDescription={t("description.rentDescription")}
            linkSite="https://car-rental-dev.vercel.app/"
            linkCode="https://github.com/Artem91S/Car_rent"
            technology={technologyRent}
          />
          <ModalContent
            key="Todo"
            photo={Todo}
            headerTitle="Board-Todo"
            description={t("description.BoostedTitle")}
            modalDescription={t("description.BoostedDescription")}
            linkSite="https://todo-board-tau.vercel.app/"
            linkCode="https://github.com/Artem91S/Board-todo/"
            technology={technologyTodo}
          />
          <ModalContent
            key="WORDPRESS THEME"
            photo={WordPressTheme}
            headerTitle="WORDPRESS THEME"
            description={t("description.WORDPRESSTitle")}
            modalDescription={t("description.WORDPRESSDescription")}
            linkSite="https://artem91s.github.io/WORDPRESS-THEME/"
            linkCode="https://github.com/Artem91S/WORDPRESS-THEME"
            technology={technologyWordPress}
          />
          <ModalContent
            key="Medical online office"
            photo={Medical}
            headerTitle="Medical online office"
            description={t("description.MedicalTitle")}
            modalDescription={t("description.MedicalDescription")}
            linkSite="https://artem91s.github.io/Medical-cabinet/"
            linkCode="https://github.com/Artem91S/Medical-cabinet"
            technology={technologyMedicalGroup}
          />
        </motion.div>
      </div>
    </>
  );
}
