import { DiBootstrap } from "react-icons/di";
import { DiCss3 } from "react-icons/di";
import { DiGulp } from "react-icons/di";
import { DiHtml5 } from "react-icons/di";
import { DiMongodb } from "react-icons/di";
import { DiNodejsSmall } from "react-icons/di";
import { DiReact } from "react-icons/di";
import { DiJsBadge } from "react-icons/di";
import { SiRedux } from "react-icons/si";
import { SiExpress } from "react-icons/si";
import { SiFirebase } from "react-icons/si";
import { SiGitlab } from "react-icons/si";
import { SiGithub } from "react-icons/si";
import { SiJest } from "react-icons/si";
import { SiNpm } from "react-icons/si";
import { SiTailwindcss } from "react-icons/si";
import { SiSass } from "react-icons/si";
import { SiFramer } from "react-icons/si";
import { SiMui } from "react-icons/si";
import { SiFluxus } from "react-icons/si";
import { motion } from "framer-motion";
import { useTranslation } from 'react-i18next';

export function Skills() {
    const { t } = useTranslation();
    const textAnimation ={
        hidden: {
            x:-30,
            opacity:0,

        },
        visible:custom=>({
            x:0,
            opacity:1,
            transition:{delay:custom*0.2}
        })
    }

    return(
    <div 
        className="w-full box-border text-white flex flex-col items-center pl-[55px] mb-[250px] lg:pl-[50px] " id='skills'>
        <motion.h1
         variants={textAnimation}
         initial='hidden'
         whileInView='visible'
         exit='hidden'
         custom={2}
        className="text-center text-white text-[39px] font-[800] sm:text-[50px] lg:text-[80px] "
        >{t('description.skills')}
        <span className='text-[#E40037] font-[900] text-[39px] sm:text-[50px] lg:text-[80px] '>.</span>
        </motion.h1>
        <a 
        className="text-center pb-[20px] underline animate-pulse w-[200px] "
        style={{textShadow:'#FFF 1px 10px 25px,'}}
        href="https://dan-it.com.ua/certificates/frontend/artem-sytnikov/" target='_blank' rel="noreferrer">{t('description.skillsLink')}</a>
        <motion.div 
         variants={textAnimation}
         initial='hidden'
         whileInView='visible'
         exit='hidden'
         custom={2}
        className='w-full grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-5 lg:gap-[30px] sm:gap-[20px] justify-center text-gray-950' >
            <motion.div
           
            className="flex flex-col items-center sm:hover:scale-125 ">
                <DiReact size={50} color='#FFF' className="w-[100px]"/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                className="text-center rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    React
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125  ">
                <SiRedux size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                className="text-center  rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Redux
                </p>
            </motion.div>
            <motion.div 
             className="flex flex-col items-center hover:scale-125">
                <DiJsBadge size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center  rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    JavaScript
                </p>
            </motion.div>
            <motion.div 
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <DiNodejsSmall size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Node js
                </p>
            </motion.div>
            <motion.div 
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <DiMongodb size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Mongo DB
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiExpress size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Express js
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiFluxus size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Formik
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiFramer size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Frame-motion
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
            <SiTailwindcss size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >tailwind</p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiMui size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    material ui
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <DiBootstrap size={50} color='#FFF' />
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    bootstrap
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiFirebase size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Firebase
                </p>
            </motion.div>
            <motion.div 
            className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiJest size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Jest
                </p>
            </motion.div>
            <motion.div 
         
            className="flex flex-col items-center sm:hover:scale-125 transition-all">
                <DiHtml5 size={50} color='#FFF' />
                <p 
                 style={{textShadow:'#FFF 1px 0 30px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    HTML
                </p>
            </motion.div>

            <motion.div 
         
            className="flex flex-col items-center sm:hover:scale-125 transition-all">
                <DiCss3 size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Css
                </p>
            </motion.div>
             <motion.div 
           
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <DiGulp size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Gulp
                </p>
            </motion.div>
            <motion.div 
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiSass size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] uppercase lg:text-[27px]"
                >
                    Sass
                </p>
            </motion.div>
            <motion.div 
            
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiNpm size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    NPM
                </p>
            </motion.div> 
            <motion.div 
             
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiGitlab size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Gitlab
                </p>
            </motion.div>

            <motion.div 
         
             className="flex flex-col items-center sm:hover:scale-125 transition-all ">
                <SiGithub size={50} color='#FFF'/>
                <p 
                 style={{textShadow:'#FFF 1px 0 15px'}}
                 className="text-center p-2 rounded fond-[800] text-[16px] md:text-[20px] uppercase lg:text-[27px]"
                >
                    Github
                </p>
            </motion.div>
           
        </motion.div>
    </div>
    )
}