import { motion ,AnimatePresence} from "framer-motion";
import { useTranslation } from 'react-i18next';

export function About() {
    const { t } = useTranslation();
    const textAnimation ={
        hidden: {
            x:-10,
            opacity:0,

        },
        visible:custom=>({
            x:0,
            opacity:1,
            transition:{delay:custom*0.4}
        })
    }
    return(
    <AnimatePresence>
            <div className='box-border flex flex-col justify-center transition-all h-[50vh] ml-[20%] mb-[380px]' id='about'>
            <motion.h1 
            variants={textAnimation}
            initial='hidden'
            whileInView='visible'
            exit='hidden'
            custom={1}
            className=" text-white text-[39px] font-[800] sm:text-[50px] lg:text-[80px]">{t('description.about')}
            <span className='text-[#E40037] font-[900] text-[39px]sm:text-[50px] lg:text-[80px]'>.</span>
            </motion.h1>
            <motion.p 
              variants={textAnimation}
              initial='hidden'
              whileInView='visible'
              exit='hidden'
              custom={1}
              className='text-white lg:text-[30px] font-[600] text-[14px] mr-[30px] sm:text-[22px]'>
              {t('description.aboutDescription')}
            </motion.p>
            </div>
            </AnimatePresence>
    )

}