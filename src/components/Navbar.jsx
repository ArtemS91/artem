import MainLogo from '../img/main.png'
import { BsFillEnvelopeAtFill } from "react-icons/bs";
import { SlSocialLinkedin } from "react-icons/sl";
import { SlSocialGithub } from "react-icons/sl";
import { Link} from 'react-scroll';
import CV from '../file/Developer_ArtemSitnikov_CV.pdf';
import CvUA from '../file/Девелопер_АртемСитніков_CV.pdf';
import  i18n  from '../Language.js';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import EN from '../img/en.png'
import UA from '../img/ua.png';

const links =[
    {
        link:'about',
        text:'navlinkFirst'   
    },
    {
        link:'skills',
        text:'navlinkSecond'   
    },
    {
        link:'portfolio',
        text:'navlinkTheerd'   
    },
    {
        link:'contacts',
        text:'navlinkFourth'   
 },
];

export function Navbar() {
    const [lang, setLang] = useState(localStorage.getItem('i18nextLng'))
    const { t } = useTranslation();
    useEffect(()=>{
      i18n.changeLanguage(lang)
    }
    ,[lang])
    return(
        <>
        <div className=' h-[70px] lg:h-[100px] w-full fixed top-0 left-0 backdrop-blur-sm z-10'>
        </div>
        <img 
        className=' fixed top-[25px] left-[170px] sm:left-[215px] lg:top-[35px] z-[20] cursor-pointer hover:scale-125'
        src={lang === 'ua'? UA: EN} alt={lang === "ua"? "Ukraine" :"English"}  onClick={(e)=>setLang(lang === 'en'? 'ua':"en")} /> 
        <div className='flex'>
        <Link 
        to='home'
        spy={true} 
        smooth={true} 
        duration={500}
        className='flex justify-start items- center '
        >
        <img className='w-[53px] h-[60px] z-20 fixed top-[10px] lg:top-[15px]' src={MainLogo} alt="Main logo"/>
        </Link>
        <a href="mailto:sitnikov.artem91@gmail.com" 
        target='_blank'
        rel="noreferrer"
        >
             <BsFillEnvelopeAtFill color='#FFF' size={30} className='fixed top-[20px] lg:top-[30px]  sm:left-[80px] left-[60px] z-20 hover:scale-110 transition-all'/>
        </a>
        <a href="https://www.linkedin.com/in/artem-sytnikov-35b55625b/" 
        target='_blank'
        rel='noreferrer'>
             <SlSocialLinkedin color='#FFF' size={30} className='fixed top-[17px] lg:top-[30px]  sm:left-[130px] left-[100px] z-20 hover:scale-110 transition-all'/>
        </a>
        <a href="https://github.com/Artem91S" 
        target='_blank'
        rel="noreferrer" >
             <SlSocialGithub color='#FFF' size={30} className='fixed top-[20px] lg:top-[30px]  sm:left-[175px] left-[135px] z-20 hover:scale-110 transition-all'/>
        </a>
            <button 
            className='bg-[#E40037] z-10 fixed text-white top-4 lg:top-5 right-2 sm:right-3 lg:right-5 rounded-xl shadow-lg hover:shadow-[#E40037]/50 transition-all hover:bg-red-900 hover:scale-90 duration-100 px-[16px] sm:px-[20px] py-[5px] text-[14px] sm:text-[20px] lg:text-[30px] lg:py-[10px]' >
                <a 
                href={lang === 'en' ? CV : CvUA }
                download={lang === 'en' ?'Developer_ArtemSitnikov_CV' : 'Девелопер_АртемСитніков_CV'}
                target='_blank'
                rel="noreferrer"
                >
                {t('description.btnText')}
                </a>
            </button>
        </div>
        <nav className="text-white bg-zinc-900 flex flex-col items-center fixed top-0 left-0 h-[100vh] px-2 " style={{writingMode:'vertical-lr'}}>
            <ul 
            className="flex gap-[30px] cursor-pointer " >
              {links.map(link=>(
                <Link
                key={link.text}
                to={link.link}
                spy={true} 
                smooth={true} 
                duration={500}
                offset={-150}
                activeClass='active'
                className='text-white p-[7px]  text-[16px]'
                >
                {t(`description.${link.text}`)}
                </Link>
              ))}
            </ul>

        </nav>
    </>
    )
}